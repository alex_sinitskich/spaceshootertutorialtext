Begin Object Class=K2Node_FunctionEntry Name="K2Node_FunctionEntry_337"
   Begin Object Class=EdGraphPin Name="EdGraphPin_30784"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_30783"
   End Object
   Begin Object Name="EdGraphPin_30784"
      PinName="Val"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      LinkedTo(0)=EdGraphPin'K2Node_CommutativeAssociativeBinaryOperator_199.EdGraphPin_32126'
   End Object
   Begin Object Name="EdGraphPin_30783"
      PinName="then"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_314.EdGraphPin_32120'
   End Object
   ExtraFlags=201457664
   SignatureName="ControlScore"
   bIsEditable=True
   Pins(0)=EdGraphPin'EdGraphPin_30783'
   Pins(1)=EdGraphPin'EdGraphPin_30784'
   NodeGuid=619A42B243632173F78D97A4685F3439
   CustomProperties UserDefinedPin Name="Val" IsArray=0 IsReference=0 Category=int 
End Object
Begin Object Class=K2Node_VariableSet Name="K2Node_VariableSet_314"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32124"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32123"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32122"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32121"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32120"
   End Object
   Begin Object Name="EdGraphPin_32124"
      PinName="Output_Get"
      PinToolTip="Retrieves the value of the variable, can use instead of a separate Get node"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4225.EdGraphPin_32140'
      LinkedTo(1)=EdGraphPin'K2Node_CallFunction_4226.EdGraphPin_32146'
   End Object
   Begin Object Name="EdGraphPin_32123"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32122"
      PinName="Score"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CommutativeAssociativeBinaryOperator_199.EdGraphPin_32128'
   End Object
   Begin Object Name="EdGraphPin_32121"
      PinName="then"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4223.EdGraphPin_32131'
   End Object
   Begin Object Name="EdGraphPin_32120"
      PinName="execute"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_FunctionEntry_337.EdGraphPin_30783'
   End Object
   VariableReference=(MemberName="Score",MemberGuid=6BDE6E044D642DD18E77FCBFCF97FEEE,bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32120'
   Pins(1)=EdGraphPin'EdGraphPin_32121'
   Pins(2)=EdGraphPin'EdGraphPin_32122'
   Pins(3)=EdGraphPin'EdGraphPin_32123'
   Pins(4)=EdGraphPin'EdGraphPin_32124'
   NodePosX=448
   NodePosY=16
   NodeGuid=435FE4AB4F9497132CA895BBC058A1E0
End Object
Begin Object Class=K2Node_CommutativeAssociativeBinaryOperator Name="K2Node_CommutativeAssociativeBinaryOperator_199"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32128"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32127"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32126"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32125"
   End Object
   Begin Object Name="EdGraphPin_32128"
      PinName="ReturnValue"
      PinToolTip="Return Value\nInteger"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_314.EdGraphPin_32122'
   End Object
   Begin Object Name="EdGraphPin_32127"
      PinName="B"
      PinToolTip="B\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_694.EdGraphPin_32129'
   End Object
   Begin Object Name="EdGraphPin_32126"
      PinName="A"
      PinToolTip="A\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_FunctionEntry_337.EdGraphPin_30784'
   End Object
   Begin Object Name="EdGraphPin_32125"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nKismet Math Library Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.KismetMathLibrary')
      DefaultObject=Default__KismetMathLibrary
      bHidden=True
   End Object
   bIsPureFunc=True
   FunctionReference=(MemberParent=Class'/Script/Engine.KismetMathLibrary',MemberName="Add_IntInt")
   Pins(0)=EdGraphPin'EdGraphPin_32125'
   Pins(1)=EdGraphPin'EdGraphPin_32126'
   Pins(2)=EdGraphPin'EdGraphPin_32127'
   Pins(3)=EdGraphPin'EdGraphPin_32128'
   NodePosX=256
   NodePosY=128
   NodeGuid=BCECCBDC41D0E53F709737AF4DEE855E
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_694"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32130"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32129"
   End Object
   Begin Object Name="EdGraphPin_32130"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32129"
      PinName="Score"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CommutativeAssociativeBinaryOperator_199.EdGraphPin_32127'
   End Object
   VariableReference=(MemberName="Score",MemberGuid=6BDE6E044D642DD18E77FCBFCF97FEEE,bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32129'
   Pins(1)=EdGraphPin'EdGraphPin_32130'
   NodePosX=64
   NodePosY=208
   NodeGuid=84967BEE4BDE272609DF99ADC7BE43B3
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4223"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32134"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32133"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32132"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32131"
   End Object
   Begin Object Name="EdGraphPin_32134"
      PinName="Value"
      PinToolTip="Value\nText (by ref)"
      PinType=(PinCategory="text",bIsReference=True,bIsConst=True)
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4225.EdGraphPin_32144'
      bDefaultValueIsIgnored=True
   End Object
   Begin Object Name="EdGraphPin_32133"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nText Render Component Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_697.EdGraphPin_32137'
   End Object
   Begin Object Name="EdGraphPin_32132"
      PinName="then"
      PinToolTip="\nExec"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_IfThenElse_220.EdGraphPin_32151'
   End Object
   Begin Object Name="EdGraphPin_32131"
      PinName="execute"
      PinToolTip="\nExec"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_314.EdGraphPin_32121'
   End Object
   FunctionReference=(MemberParent=Class'/Script/Engine.TextRenderComponent',MemberName="K2_SetText")
   Pins(0)=EdGraphPin'EdGraphPin_32131'
   Pins(1)=EdGraphPin'EdGraphPin_32132'
   Pins(2)=EdGraphPin'EdGraphPin_32133'
   Pins(3)=EdGraphPin'EdGraphPin_32134'
   NodePosX=912
   NodePosY=-16
   NodeGuid=10CA82994E745C7892EA1881C55FC2B1
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_697"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32138"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32137"
   End Object
   Begin Object Name="EdGraphPin_32138"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32137"
      PinName="ScoreTextNumber"
      Direction=EGPD_Output
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4223.EdGraphPin_32133'
   End Object
   VariableReference=(MemberName="ScoreTextNumber",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32137'
   Pins(1)=EdGraphPin'EdGraphPin_32138'
   NodePosX=960
   NodePosY=144
   NodeGuid=AB3D784447D67D4C0428179F81521EAC
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4225"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32144"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32143"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32142"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32141"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32140"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32139"
   End Object
   Begin Object Name="EdGraphPin_32144"
      PinName="ReturnValue"
      PinToolTip="Return Value\nText"
      Direction=EGPD_Output
      PinType=(PinCategory="text")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4223.EdGraphPin_32134'
   End Object
   Begin Object Name="EdGraphPin_32143"
      PinName="MaximumIntegralDigits"
      PinToolTip="Maximum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="324"
      AutogeneratedDefaultValue="324"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32142"
      PinName="MinimumIntegralDigits"
      PinToolTip="Minimum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="1"
      AutogeneratedDefaultValue="1"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32141"
      PinName="bUseGrouping"
      PinToolTip="Use Grouping\nBoolean"
      PinType=(PinCategory="bool")
      DefaultValue="true"
      AutogeneratedDefaultValue="true"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32140"
      PinName="Value"
      PinToolTip="Value\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_314.EdGraphPin_32124'
   End Object
   Begin Object Name="EdGraphPin_32139"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nKismet Text Library Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.KismetTextLibrary')
      DefaultObject=Default__KismetTextLibrary
      bHidden=True
   End Object
   bIsPureFunc=True
   FunctionReference=(MemberParent=Class'/Script/Engine.KismetTextLibrary',MemberName="Conv_IntToText")
   Pins(0)=EdGraphPin'EdGraphPin_32139'
   Pins(1)=EdGraphPin'EdGraphPin_32140'
   Pins(2)=EdGraphPin'EdGraphPin_32141'
   Pins(3)=EdGraphPin'EdGraphPin_32142'
   Pins(4)=EdGraphPin'EdGraphPin_32143'
   Pins(5)=EdGraphPin'EdGraphPin_32144'
   NodePosX=640
   NodePosY=96
   NodeGuid=7EDEBA624A300C8D1DAE06BCEEDE5B58
   AdvancedPinDisplay=Hidden
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4226"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32148"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32147"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32146"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32145"
   End Object
   Begin Object Name="EdGraphPin_32148"
      PinName="ReturnValue"
      PinToolTip="Return Value\nBoolean"
      Direction=EGPD_Output
      PinType=(PinCategory="bool")
      AutogeneratedDefaultValue="false"
      LinkedTo(0)=EdGraphPin'K2Node_IfThenElse_220.EdGraphPin_32152'
   End Object
   Begin Object Name="EdGraphPin_32147"
      PinName="B"
      PinToolTip="B\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_699.EdGraphPin_32149'
   End Object
   Begin Object Name="EdGraphPin_32146"
      PinName="A"
      PinToolTip="A\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_314.EdGraphPin_32124'
   End Object
   Begin Object Name="EdGraphPin_32145"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nKismet Math Library Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.KismetMathLibrary')
      DefaultObject=Default__KismetMathLibrary
      bHidden=True
   End Object
   bIsPureFunc=True
   FunctionReference=(MemberParent=Class'/Script/Engine.KismetMathLibrary',MemberName="GreaterEqual_IntInt")
   Pins(0)=EdGraphPin'EdGraphPin_32145'
   Pins(1)=EdGraphPin'EdGraphPin_32146'
   Pins(2)=EdGraphPin'EdGraphPin_32147'
   Pins(3)=EdGraphPin'EdGraphPin_32148'
   NodePosX=688
   NodePosY=272
   NodeGuid=26E2193E45494B151CF9D89699F0DDB1
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_699"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32150"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32149"
   End Object
   Begin Object Name="EdGraphPin_32150"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32149"
      PinName="BestScore"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4226.EdGraphPin_32147'
   End Object
   VariableReference=(MemberName="BestScore",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32149'
   Pins(1)=EdGraphPin'EdGraphPin_32150'
   NodePosX=496
   NodePosY=320
   NodeGuid=9D7656504AFAA309562D749874121B9F
End Object
Begin Object Class=K2Node_IfThenElse Name="K2Node_IfThenElse_220"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32154"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32153"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32152"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32151"
   End Object
   Begin Object Name="EdGraphPin_32154"
      PinName="else"
      PinFriendlyName="false"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_315.EdGraphPin_32174'
   End Object
   Begin Object Name="EdGraphPin_32153"
      PinName="then"
      PinFriendlyName="true"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_316.EdGraphPin_32155'
   End Object
   Begin Object Name="EdGraphPin_32152"
      PinName="Condition"
      PinType=(PinCategory="bool")
      DefaultValue="false"
      AutogeneratedDefaultValue="false"
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4226.EdGraphPin_32148'
   End Object
   Begin Object Name="EdGraphPin_32151"
      PinName="execute"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4223.EdGraphPin_32132'
   End Object
   Pins(0)=EdGraphPin'EdGraphPin_32151'
   Pins(1)=EdGraphPin'EdGraphPin_32152'
   Pins(2)=EdGraphPin'EdGraphPin_32153'
   Pins(3)=EdGraphPin'EdGraphPin_32154'
   NodePosX=1296
   NodeGuid=B9C292C04A8A65552C11B397FA21D3C5
End Object
Begin Object Class=K2Node_VariableSet Name="K2Node_VariableSet_316"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32159"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32158"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32157"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32156"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32155"
   End Object
   Begin Object Name="EdGraphPin_32159"
      PinName="Output_Get"
      PinToolTip="Retrieves the value of the variable, can use instead of a separate Get node"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4231.EdGraphPin_32169'
   End Object
   Begin Object Name="EdGraphPin_32158"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32157"
      PinName="BestScore"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_703.EdGraphPin_32160'
   End Object
   Begin Object Name="EdGraphPin_32156"
      PinName="then"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4230.EdGraphPin_32162'
   End Object
   Begin Object Name="EdGraphPin_32155"
      PinName="execute"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_IfThenElse_220.EdGraphPin_32153'
   End Object
   VariableReference=(MemberName="BestScore",MemberGuid=3386784D485F08E8DDB4AC86C33637FF,bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32155'
   Pins(1)=EdGraphPin'EdGraphPin_32156'
   Pins(2)=EdGraphPin'EdGraphPin_32157'
   Pins(3)=EdGraphPin'EdGraphPin_32158'
   Pins(4)=EdGraphPin'EdGraphPin_32159'
   NodePosX=1584
   NodePosY=-80
   NodeGuid=EE00FE61430B988FF873B6B3B91BF8FF
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_703"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32161"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32160"
   End Object
   Begin Object Name="EdGraphPin_32161"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32160"
      PinName="Score"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_316.EdGraphPin_32157'
   End Object
   VariableReference=(MemberName="Score",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32160'
   Pins(1)=EdGraphPin'EdGraphPin_32161'
   NodePosX=1584
   NodePosY=16
   NodeGuid=4D9073224278B0442C7205A5013B59D2
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4230"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32165"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32164"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32163"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32162"
   End Object
   Begin Object Name="EdGraphPin_32165"
      PinName="Value"
      PinToolTip="Value\nText (by ref)"
      PinType=(PinCategory="text",bIsReference=True,bIsConst=True)
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4231.EdGraphPin_32173'
      bDefaultValueIsIgnored=True
   End Object
   Begin Object Name="EdGraphPin_32164"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nText Render Component Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_704.EdGraphPin_32166'
   End Object
   Begin Object Name="EdGraphPin_32163"
      PinName="then"
      PinToolTip="\nExec"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
   End Object
   Begin Object Name="EdGraphPin_32162"
      PinName="execute"
      PinToolTip="\nExec"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_316.EdGraphPin_32156'
   End Object
   FunctionReference=(MemberParent=Class'/Script/Engine.TextRenderComponent',MemberName="K2_SetText")
   Pins(0)=EdGraphPin'EdGraphPin_32162'
   Pins(1)=EdGraphPin'EdGraphPin_32163'
   Pins(2)=EdGraphPin'EdGraphPin_32164'
   Pins(3)=EdGraphPin'EdGraphPin_32165'
   NodePosX=2048
   NodePosY=-112
   NodeGuid=D67F3B8E474C1BF3209D47950ABCFEDB
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_704"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32167"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32166"
   End Object
   Begin Object Name="EdGraphPin_32167"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32166"
      PinName="BestScoreNumber"
      Direction=EGPD_Output
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4230.EdGraphPin_32164'
   End Object
   VariableReference=(MemberName="BestScoreNumber",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32166'
   Pins(1)=EdGraphPin'EdGraphPin_32167'
   NodePosX=2096
   NodePosY=32
   NodeGuid=7AEF71424751DC539EE18298AA4F1505
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4231"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32173"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32172"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32171"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32170"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32169"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32168"
   End Object
   Begin Object Name="EdGraphPin_32173"
      PinName="ReturnValue"
      PinToolTip="Return Value\nText"
      Direction=EGPD_Output
      PinType=(PinCategory="text")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4230.EdGraphPin_32165'
   End Object
   Begin Object Name="EdGraphPin_32172"
      PinName="MaximumIntegralDigits"
      PinToolTip="Maximum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="324"
      AutogeneratedDefaultValue="324"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32171"
      PinName="MinimumIntegralDigits"
      PinToolTip="Minimum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="1"
      AutogeneratedDefaultValue="1"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32170"
      PinName="bUseGrouping"
      PinToolTip="Use Grouping\nBoolean"
      PinType=(PinCategory="bool")
      DefaultValue="true"
      AutogeneratedDefaultValue="true"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32169"
      PinName="Value"
      PinToolTip="Value\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_316.EdGraphPin_32159'
   End Object
   Begin Object Name="EdGraphPin_32168"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nKismet Text Library Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.KismetTextLibrary')
      DefaultObject=Default__KismetTextLibrary
      bHidden=True
   End Object
   bIsPureFunc=True
   FunctionReference=(MemberParent=Class'/Script/Engine.KismetTextLibrary',MemberName="Conv_IntToText")
   Pins(0)=EdGraphPin'EdGraphPin_32168'
   Pins(1)=EdGraphPin'EdGraphPin_32169'
   Pins(2)=EdGraphPin'EdGraphPin_32170'
   Pins(3)=EdGraphPin'EdGraphPin_32171'
   Pins(4)=EdGraphPin'EdGraphPin_32172'
   Pins(5)=EdGraphPin'EdGraphPin_32173'
   NodePosX=1824
   NodePosY=-16
   NodeGuid=FB887DC34BA7AC1FB3A484BFFF987E97
   AdvancedPinDisplay=Hidden
End Object
Begin Object Class=K2Node_VariableSet Name="K2Node_VariableSet_315"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32178"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32177"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32176"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32175"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32174"
   End Object
   Begin Object Name="EdGraphPin_32178"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32177"
      PinName="Output_Get"
      PinToolTip="Retrieves the value of the variable, can use instead of a separate Get node"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4229.EdGraphPin_32214'
   End Object
   Begin Object Name="EdGraphPin_32176"
      PinName="BestScore"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_706.EdGraphPin_32231'
   End Object
   Begin Object Name="EdGraphPin_32175"
      PinName="then"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4227.EdGraphPin_32195'
   End Object
   Begin Object Name="EdGraphPin_32174"
      PinName="execute"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_IfThenElse_220.EdGraphPin_32154'
   End Object
   VariableReference=(MemberName="BestScore",MemberGuid=3386784D485F08E8DDB4AC86C33637FF,bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32174'
   Pins(1)=EdGraphPin'EdGraphPin_32175'
   Pins(2)=EdGraphPin'EdGraphPin_32176'
   Pins(3)=EdGraphPin'EdGraphPin_32177'
   Pins(4)=EdGraphPin'EdGraphPin_32178'
   NodePosX=1600
   NodePosY=176
   NodeGuid=539C5696486B4DBFFCCA96907DCCDE29
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4227"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32198"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32197"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32196"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32195"
   End Object
   Begin Object Name="EdGraphPin_32198"
      PinName="Value"
      PinToolTip="Value\nText (by ref)"
      PinType=(PinCategory="text",bIsReference=True,bIsConst=True)
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4229.EdGraphPin_32218'
      bDefaultValueIsIgnored=True
   End Object
   Begin Object Name="EdGraphPin_32197"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nText Render Component Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_VariableGet_702.EdGraphPin_32207'
   End Object
   Begin Object Name="EdGraphPin_32196"
      PinName="then"
      PinToolTip="\nExec"
      Direction=EGPD_Output
      PinType=(PinCategory="exec")
   End Object
   Begin Object Name="EdGraphPin_32195"
      PinName="execute"
      PinToolTip="\nExec"
      PinType=(PinCategory="exec")
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_315.EdGraphPin_32175'
   End Object
   FunctionReference=(MemberParent=Class'/Script/Engine.TextRenderComponent',MemberName="K2_SetText")
   Pins(0)=EdGraphPin'EdGraphPin_32195'
   Pins(1)=EdGraphPin'EdGraphPin_32196'
   Pins(2)=EdGraphPin'EdGraphPin_32197'
   Pins(3)=EdGraphPin'EdGraphPin_32198'
   NodePosX=2064
   NodePosY=144
   NodeGuid=C14EC4624078C3566653C4BE5B226BFC
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_702"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32208"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32207"
   End Object
   Begin Object Name="EdGraphPin_32208"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32207"
      PinName="BestScoreNumber"
      Direction=EGPD_Output
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.TextRenderComponent')
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4227.EdGraphPin_32197'
   End Object
   VariableReference=(MemberName="BestScoreNumber",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32207'
   Pins(1)=EdGraphPin'EdGraphPin_32208'
   NodePosX=2112
   NodePosY=288
   NodeGuid=695D495243F8BEA9D85D038C599C6BEC
End Object
Begin Object Class=K2Node_CallFunction Name="K2Node_CallFunction_4229"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32218"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32217"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32216"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32215"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32214"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32213"
   End Object
   Begin Object Name="EdGraphPin_32218"
      PinName="ReturnValue"
      PinToolTip="Return Value\nText"
      Direction=EGPD_Output
      PinType=(PinCategory="text")
      LinkedTo(0)=EdGraphPin'K2Node_CallFunction_4227.EdGraphPin_32198'
   End Object
   Begin Object Name="EdGraphPin_32217"
      PinName="MaximumIntegralDigits"
      PinToolTip="Maximum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="324"
      AutogeneratedDefaultValue="324"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32216"
      PinName="MinimumIntegralDigits"
      PinToolTip="Minimum Integral Digits\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="1"
      AutogeneratedDefaultValue="1"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32215"
      PinName="bUseGrouping"
      PinToolTip="Use Grouping\nBoolean"
      PinType=(PinCategory="bool")
      DefaultValue="true"
      AutogeneratedDefaultValue="true"
      bAdvancedView=True
   End Object
   Begin Object Name="EdGraphPin_32214"
      PinName="Value"
      PinToolTip="Value\nInteger"
      PinType=(PinCategory="int")
      DefaultValue="0"
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_315.EdGraphPin_32177'
   End Object
   Begin Object Name="EdGraphPin_32213"
      PinName="self"
      PinFriendlyName="Target"
      PinToolTip="Target\nKismet Text Library Reference"
      PinType=(PinCategory="object",PinSubCategoryObject=Class'/Script/Engine.KismetTextLibrary')
      DefaultObject=Default__KismetTextLibrary
      bHidden=True
   End Object
   bIsPureFunc=True
   FunctionReference=(MemberParent=Class'/Script/Engine.KismetTextLibrary',MemberName="Conv_IntToText")
   Pins(0)=EdGraphPin'EdGraphPin_32213'
   Pins(1)=EdGraphPin'EdGraphPin_32214'
   Pins(2)=EdGraphPin'EdGraphPin_32215'
   Pins(3)=EdGraphPin'EdGraphPin_32216'
   Pins(4)=EdGraphPin'EdGraphPin_32217'
   Pins(5)=EdGraphPin'EdGraphPin_32218'
   NodePosX=1840
   NodePosY=240
   NodeGuid=10F283CA49778C62BF74BE92126B5BC1
   AdvancedPinDisplay=Hidden
End Object
Begin Object Class=K2Node_VariableGet Name="K2Node_VariableGet_706"
   Begin Object Class=EdGraphPin Name="EdGraphPin_32232"
   End Object
   Begin Object Class=EdGraphPin Name="EdGraphPin_32231"
   End Object
   Begin Object Name="EdGraphPin_32232"
      PinName="self"
      PinFriendlyName="Target"
      PinType=(PinCategory="object",PinSubCategoryObject=BlueprintGeneratedClass'/Game/Blueprint/BPGameController.BPGameController_C')
      bHidden=True
   End Object
   Begin Object Name="EdGraphPin_32231"
      PinName="BestScore"
      Direction=EGPD_Output
      PinType=(PinCategory="int")
      AutogeneratedDefaultValue="0"
      LinkedTo(0)=EdGraphPin'K2Node_VariableSet_315.EdGraphPin_32176'
   End Object
   VariableReference=(MemberName="BestScore",bSelfContext=True)
   Pins(0)=EdGraphPin'EdGraphPin_32231'
   Pins(1)=EdGraphPin'EdGraphPin_32232'
   NodePosX=1600
   NodePosY=288
   NodeGuid=8D9B011649E0B98A442F8E80876B6D8D
End Object
